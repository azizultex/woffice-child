<?php
function woffice_child_scripts() {
	if ( ! is_admin() && ! in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) ) ) {
		$theme_info = wp_get_theme();
		wp_enqueue_style( 'woffice-child-stylesheet', get_stylesheet_uri(), array(), $theme_info->get( 'Version' ) );
	}
}
add_action('wp_enqueue_scripts', 'woffice_child_scripts', 30);


/**
 * customize login screen
 *
 */
add_filter( 'login_headerurl', 'viz360_partners_custom_login_page' );
add_filter( 'login_headertitle', 'viz360_partners_login_logo_url_title' );
add_action('login_head', 'viz360_partners_custom_login_logo_url');

function viz360_partners_custom_login_page() {
    echo '<style type="text/css">
        h1 a { background-image:url("'. get_stylesheet_directory_uri().'/assets/img/viz360-partners.png") !important; height: 75px !important; width: 100% !important; margin: 0 auto !important; background-size: contain !important; }
		h1 a:focus { outline: 0 !important; box-shadow: none; }
        body.login { background-image:url("'. get_stylesheet_directory_uri().'/assets/img/login-background.jpg") !important; background-repeat: no-repeat !important; background-attachment: fixed !important; background-position: center !important; background-size: cover !important; position: relative; z-index: 999;}
  		body.login:before { background-color: rgba(0, 0, 0, 0.5); position: absolute; width: 100%; height: 100%; left: 0; top: 0; content: ""; z-index: -1; }
  		.login form {
  			background: rgba(255,255,255, 0.2) !important;
  		}
		.login form .input, .login form input[type=checkbox], .login input[type=text] {
			background: transparent !important;
			color: #ddd;
		}
		.login label {
			color: #DDD !important;
		}
		.login #login_error, .login .message {
			color: #ddd;
			margin-top: 20px;
			background: rgba(255,255,255, 0.2) !important;
		}
		.login #backtoblog a, .login #nav a {
			color: #808080;
		}
    </style>';
}

function viz360_partners_login_logo_url_title() {
 	return 'Viz360 - 3D Virtual Tours &amp; Visualisation for Architecture, Developers &amp; Real Estate';
}

function viz360_partners_custom_login_logo_url() {
	return get_bloginfo( 'url' );
}
